<?php

include('../dbconfig.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <title>Crete a user</title>
</head>
<body>

<div class="container pt-3" id="registration">
    
    <h2 class="text-center" style="font-size: 20px;">Create an Account as Individual Company</h2>
    <div class="row mt-4 mb-4">
        <div class="col-md-6 offset-md-3">
            <form id="reg_form" action='' class="shadow-lg p-4" method="POST">

                <div class="form-group">
                    <i class="fas fa-user"></i><label for="name" class="pl-2 font-weight-bold" style="margin-bottom: 05px;">Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                </div>
                <div class="form-group">
                    <i class="fas fa-user"></i>
                    <label for="name" class="pl-2 my-2 font-weight-bold">Company Name</label>
                <select name="companyName" id="companyName" class="form-select" aria-label="Default select example" style="margin-bottom: 10px;"  >
                 
                <option value='' disabled selected>Select</option>
                 <?php
                          $sql = "SELECT * FROM company";
                          $result = $conn->query($sql);

                          if ($result->num_rows > 0) {
                              while ($row = $result->fetch_assoc()) {
                                  echo '<option value="' . $row['id'] . '">' . $row['companyName'] . '</option>';
                              }
                          }
                          ?>
               </select>
               </div>
                <div class="form-group">
                    <i class="fas fa-user"></i><label for="email" class="font-weight-bold" style="margin-bottom: 05px; margin-top: 0px; !important">Email</label><input
                        type="email" id="email" class="form-control" placeholder="Email" name="email" >
                    <!--Add text-white below if want text color white-->
                    <small class="form-text">We'll never share your email with anyone else.</small>
                </div>
                 <div class="form-group">
                    <i class="fas fa-user"></i><label for="phone" class="pl-2 font-weight-bold" style="margin-bottom: 05px;">Phone</label>
                    <input
                        type="number" class="form-control" placeholder="Phone number" name="phone" id="phone" required >
                    <!--Add text-white below if want text color white-->
                </div>
                <div class="form-group">
                    <i class="fas fa-key"></i><label for="pass" class="pl-2 font-weight-bold" style="margin-bottom: 05px; margin-top: 05px;">Password</label>
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" >
                </div>
                <button type="submit" class="btn btn-danger mt-5 btn-block shadow-sm font-weight-bold"
                    name="ExecutiveSignup" id="submit">Sign Up</button>
                <em style="font-size:10px; margin-left: 20px;">Note - By clicking Sign Up, you agree to our Terms, Data
                    Policy and Cookie Policy.</em>
                <div class="text-center">
                    <p style="font-size:12px; margin-bottom: 05px !important;">Already Have an Account?</p>
                </div>
                <div class="text-center"><a class="btn btn-success shadow-sm font-weight-bold"
                        href="login.php" style="margin-bottom: 05px;">Login</a>
                </div>
                <div id="message"></div>
            </form>
        </div>
    </div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
    <!-- <script src="../js/registerUser.js"></script> -->
    </body>
<script>

$(document).ready(function(){
    
//   $("#submit").click(function(){
  $('#reg_form').submit(function () {     
// alert('ok');
    var name = $("#name").val();
    var companyName = $("#companyName").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var role = 3;
    var status = 1;

    if ((email == "") || (password == "") || (name == "") || (companyName == "") || (phone == "") ) {
      
           $("#message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong> Error! </strong> Please enter all the fields <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button ></div >' )
    }
    else {
      $.ajax({
        type: "POST",
        url: "createuser.php",
        data: "&name=" + name + "&companyName=" + companyName + "&email=" + email + "&password=" + password + "&phone=" + phone + "&role=" + role + "&status=" + status ,
        success: function(res){
            console    
          if (res=='true') {
             $("#message").html('<div class="alert alert-success alert-dismissible fade show" role="alert"><strong> Success! </strong> User hasbeen created Successfully. <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button ></div >');
          }
          else {
            $("#message").html(res);
          }
        },
        beforeSend:function()
        {
          $("#message").html("<p class='text-center'>Processing...</p>")
        },
        error: function(xhr, status, error) {
                        console.log(111, xhr.responseText);
                        console.log(error);
                        console.log(status);
                    }
      });
    }
    return false;
  });
});
// $('#retypepwd').on('keyup', function () {
//     if ($(this).val() == $('#mypassword').val()) {
//         document.getElementById("submit").disabled = false;
//     } else{
//        document.getElementById("submit").disabled = true;
//        $('#message').html('Passwords don\'t match').css('color', 'red');
//   }
// });
</script>
</html>