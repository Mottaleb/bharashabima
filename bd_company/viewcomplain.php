<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="../styles/styles.css">
</head>
<body>

<div class="container mt-5">

            <select name="filter" id="filter" class="form-select-lg mb-3" aria-label=".form-select-lg example">        
              <option value="all">All</option>
              <option value="1">Metlife</option>
              <option value="2">Farest</option>
              <option value="3">Padma</option>
          </select>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            All complaints
                           
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="message-show">

                        </div>
                        <table class="table table-bordered table-striped table-fixed">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Mobile number</th>
                                    <th>Email</th>
                                    <th>Pin code</th>
                                    <th>complaint</th>
                                </tr>
                            </thead>
                            <tbody class="studentdata" id="studentdata">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script>
    // $(document).ready(function() {

    //       getdata()

    // });
    //  function getdata()
    //     {
    //         $.ajax({
    //             type: "GET",
    //             url: "viewallData.php",
    //             success: function (response) {
    //                 // console.log(response);
    //                 $.each(response, function (key, value) { 
    //                     // console.log(value['fname']);
    //                     $('.studentdata').append('<tr>'+
    //                             '<td class="stud_id">'+value['id']+'</td>\
    //                             <td>'+value['name']+'</td>\
    //                             <td>'+value['mobile_number']+'</td>\
    //                             <td>'+value['email']+'</td>\
    //                             <td>'+value['code']+'</td>\
    //                             <td>\
    //                                 <a href="#" class="badge btn-info viewbtn">VIEW</a>\
    //                                 <a href="#" class="badge btn-primary edit_btn">EDIT</a>\
    //                                 <a href="" class="badge btn-danger">Delete</a>\
    //                             </td>\
    //                         </tr>');
    //                 });
    //             }
    //         });
    //     }
  
  $(document).ready(function () {

                        updateTable('all');
            function updateTable(filter) {
                $.ajax({
                    type: "POST",
                    url: "viewallData.php",
                    dataType: 'json',
                    data: { filter: filter },
                    success: function (res) {

                    //var getres = $.parseJSON(res);
                    //console.log(101,getres);
                        var tableBody = $("#studentdata");
                        tableBody.empty();
                        $.each(res, function (index, item) {
                            tableBody.append("<tr><td>" 
                            + item.name + "</td><td>" 
                            + item.mobile_number + "</td><td>" 
                            + item.email + "</td><td>" 
                            + item.code + "</td><td>"
                            + item.complaint_descr + "</td>"
                            
                            
                            +"</tr>");
                        });
                    },
                    error: function(xhr, status, error) {
                        console.log(111, xhr.responseText);
                        // console.log(error);
                        // console.log(status);
                    }
                });
            }

            // Initial call to populate the table


            // Event listener for select option change
            $("#filter").on("change", function () {
                var selectedFilter = $(this).val();
                updateTable(selectedFilter);
            });
        });
  
  
  </script>
</body>
</html>