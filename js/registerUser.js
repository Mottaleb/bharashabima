$(document).ready(function(){
    
  $("#submit").click(function(){

    var name = $("#name").val();
    var companyName = $("#companyName").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var role = 3;
    var status = 1;
    
    
    if ((email == "") || (password == "") || (name == "") || (companyName == "") || (phone == "") ) {
      
      // $("#message").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Please enter all the fields</div>");
    
      $("#message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong> Error! </strong> Please enter all the fields <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" ></button ></div >' )

    }
    else {
      $.ajax({
        type: "POST",
        url: "createuser.php",
        data: "&name=" + name + "&companyName=" + companyName + "&email=" + email + "&password=" + password + "&phone=" + phone + "&role=" + role + "&status=" + status ,
        success: function(res){    
          if (res=='true') {
            window.location="index.php";
          }
          else {
            $("#message").html(res);
          }
        },
        beforeSend:function()
        {
          $("#message").html("<p class='text-center'>Processing...</p>")
        }
      });
    }
    return false;
  });
});
// $('#retypepwd').on('keyup', function () {
//     if ($(this).val() == $('#mypassword').val()) {
//         document.getElementById("submit").disabled = false;
//     } else{
//        document.getElementById("submit").disabled = true;
//        $('#message').html('Passwords don\'t match').css('color', 'red');
//   }
// });